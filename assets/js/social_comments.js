jQuery(document).ready(function() {

    jQuery('.new-post-form .form-textarea-wrapper textarea').autosize();

    jQuery('.comment-reply > a').click(function(e){
        $(this).parent().parent().siblings('.add-post-comment-form').slideToggle('5000');
        e.preventDefault();
    });

    jQuery('.new-post-form .form-textarea-wrapper textarea').keypress(function(e) {
        if(e.ctrlKey && e.keyCode == 13) {
            alert('pressed ctrl+return');
            $(this).parent().parent().parent().parent('form').submit();
            e.preventDefault();
        }
    });

    var placeholder;

    jQuery('.form-textarea-wrapper textarea').focusin(function(){
        placeholder = jQuery(this).attr('placeholder');
        jQuery(this).attr('placeholder','');
    });

    jQuery('.form-textarea-wrapper textarea').focusout(function(){
        jQuery(this).attr('placeholder',placeholder);
    });


});
