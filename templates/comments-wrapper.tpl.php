<div class='social-comments-wrapper'>
  <div class='new-social-comments-form'>
<?php print ''; ?>
  </div>
  <div id='new-social-comments-ajax-container'></div>
  <?php if (isset($comments_list)): ?>
  <div id="social-comments-list">
    <?php print render ($comments_list); ?>
  </div>
  <?php endif; ?>
</div>
