<div class='topic-container'>
  <!--Main comment container-->
  <div class='main-comment-container'>
    <?php print render($main_comment); ?>
  </div> <!--End of Main Comment container-->
  <!--Child Comments Container-->
  <div class='child-comments-container'>
    <?php if($children){
      print render($children);
    } ?>
  </div> <!-- End Of Child Comments Container-->
</div>
