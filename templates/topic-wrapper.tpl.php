<div <?php print $topic_attributes; ?>>
  <!--Main comment container-->
  <div class='main-comment-container'>
    <?php print $main_comment; ?>
  </div> <!--End of Main Comment container-->
  <!--Child Comments Container-->
  <?php if($child_comments) : ?>
    <div class='child-comments-container'>
      <?php print $child_comments; ?>
    </div>
  <?php endif; ?><!-- End Of Child Comments Container-->
</div>
