<?php
/**
 * @file
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->created variable.
 * - $changed: Formatted date and time for when the comment was last changed.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->changed variable.
 * - $new: New comment marker.
 * - $permalink: Comment permalink.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $title: Linked title.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - comment: The current template type, i.e., "theming hook".
 *   - comment-by-anonymous: Comment by an unregistered user.
 *   - comment-by-node-author: Comment by the author of the parent node.
 *   - comment-preview: When previewing a new or edited comment.
 *   The following applies only to viewers who are registered users:
 *   - comment-unpublished: An unpublished comment visible only to administrators.
 *   - comment-by-viewer: Comment by the user currently viewing the page.
 *   - comment-new: New comment since last the visit.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * These two variables are provided for context:
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_comment()
 * @see template_process()
 * @see theme_comment()
 * @see adaptivetheme_preprocess_comment()
 * @see adaptivetheme_process_comment()
 *
 * Hiding Content and Printing it Separately:
 * Use the hide() function to hide fields and other content, you can render it
 * later using the render() function. Install the Devel module and use
 * <?php print dsm($content); ?> to find variable names to hide() or render().
 */

?>
<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
   <?php if($picture): ?>
      <section class='image-container'>
        <?php print $picture; ?>
      </section>
    <?php endif; ?>

    <section class='comment-body-with-author'>
      <header class='comment-header'>
        <span class='author-name'><?php print $author; ?></span>
        <span class='comment-body'>
          <?php
          hide($content['links']);
          print render($content['comment_body']['#items'][0]['value']);
          ?>
        </span>
        <span class='comment-date-created'><?php print render($date_created); ?></span>
      </header>
      <footer class='comment-footer'>
        <?php print render($content['links']); ?>
        <?php if($custom_comment_form): ?>
        <div class='add-post-comment-form'>
          <div class='new-post-picture'>
              <?php print $picture; ?>
          </div>
          <div class='new-post-form'>
            <?php print drupal_render($custom_comment_form); ?>
          </div>
        </div>
        <?php endif ?>
      </footer>
    </section>

</article>
