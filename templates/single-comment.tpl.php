<div <?php print $comment_attributes; ?>>
  <!--Single comment template-->
  <div class='social-comment-user-picture-container'>
    <?php print $user_picture; ?>
  </div>
  <div class='social-comment-content'>
    <?php if ($is_main){ ?>
      <?php print ("<span class='social-comment-user-label'>" . $username . "</span>"); ?>
      <?php print ("<span class='social-comment-date-created'>" . $created . "</span>"); ?>
      <?php print ("<span class='comment-body'>" . $body . "</span>"); ?>
    <?php } else { ?>
      <?php print ("<span class='social-comment-user-label'>" . $username . "</span>"); ?>
      <?php print ("<span class='comment-body'>" . $body . "</span>"); ?>
      <?php print ("<span class='social-comment-date-created'>" . $created . "</span>"); ?>
    <?php } ?>
  </div>
  <?php if (!empty($links)): ?>
  <div class='social-comment-links'>
    <?php print $links; ?>
  </div>
  <?php endif ?>
</div>
<?php if($last): ?>
  <div class='social-comments-ajax-container'></div>
  <div class='social-comments-form-container'>
    <?php if (isset($current_user_picture)): ?>
    <div class='social-comments-user-picture-container'>
      <?php print $current_user_picture ?>
    </div>
    <?php endif; ?>
    <?php print $reply_form; ?>
  </div>
<?php endif ?>
